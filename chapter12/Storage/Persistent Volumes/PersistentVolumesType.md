##    PVC类型
    GCEPersistentDisk
    AWSElasticBlockStore
    AzureFile
    AzureDisk
    FC (Fibre Channel)
    FlexVolume
    Flocker
    NFS
    iSCSI
    RBD (Ceph Block Device)
    CephFS
    Cinder (OpenStack block storage)
    Glusterfs
    VsphereVolume
    Quobyte Volumes
    HostPath
    VMware Photon
    Portworx Volumes
    ScaleIO Volumes
    StorageOS
