#!/bin/bash
##定义 Nginx 日志路径为/data/logs/nginx/
##__author__ is humingzhe
##email admin@humingzhe.com

# Nginx日志切割

d=`date -d "-1 day" +%Y%m%d `
logdir="/data/logs/nginx/"
nginx_pid="/usr/local/nginx/logs/nginx.pid"
cd $logdir
for log in `ls *.log`
do
	mv $log $d-$log
done
/bin/kill -HUP `cat $nginx_pid`